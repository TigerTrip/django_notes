"""
WSGI config for django_notes project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application
os.environ['PYTHON_EGG_CACHE'] = '/<a path>/.python-eggs/' #为了防止Permission denied的web请求访问错误。
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "django_notes.settings")

application = get_wsgi_application()
