from django.shortcuts import render, HttpResponse
import json
# Create your views here.


def index(request):
    # return HttpResponse('hello world')
    return render(request, 'index.html')


def login(request):
    resp = {
        'state': 200,
        'msg': ''
    }
    return HttpResponse(json.dumps(resp), content_type="application/json")
